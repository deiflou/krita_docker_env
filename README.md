This is a docker environment intended for ease of use when developing small
features and for when starting coding in Krita.

This system assumes that you have some src folder in your machine with Krita's
source code. In fact you can have a general folder and inside several worktrees
that can be built separately inside the docker container without clashing. This
folder is mounted in the container so there is no need for copying any source
files.

Docker container setup
======================

There are a set of scripts that ease the process of setting up the environment.
We execute them in sequence. These steps need to be executed anytime one wants
to recreate the image. For example when downloading new dependencies.

1. First of all we need to download the precompiled dependencies that Krita
   needs to run:
   
   `./download_latest_deps`
   
   That command downloads the packaged dependencies inside `./image/latest_deps/`.
   For it to work you need to be connected to internet. You can also download
   the package manually from [https://binary-factory.kde.org/job/Krita_Nightly_Appimage_Dependency_Build/lastSuccessfulBuild/artifact/krita-appimage-deps.tar](https://binary-factory.kde.org/job/Krita_Nightly_Appimage_Dependency_Build/lastSuccessfulBuild/artifact/krita-appimage-deps.tar). If you do that, put the package inside `./image/latest_deps/`.
   
   This step can take a while to complete.

2. Next we need to build the docker image that contains the linux distribution
   and all the facilities for building Krita. Then the container will be created
   based on this image:
   
   `./build_image [IMAGE_NAME]`
   
   Substitute **[IMAGE_NAME]** with some sensible name. I like to use this pattern:
   `krita_dev_env_[DEPENDENCIES_BUILD_DATE]`, where **[DEPENDENCIES_BUILD_DATE]**
   is the date when the prebuilt dependencies that are going to be used were built.
   For example, `krita_dev_env_2024-01-26`. That way it is easy to differentiate
   the images, in case you have more than one. But you can use whatever name you like.
   
   This step can take a while to complete.

3. Now we have to create a container based on that image. This step may sound
   unnecessary at first, but keep in mind that one can have multiple containers
   based on the same image and use each one for different things. Here we want
   just to build Krita so a unique container will be created. We need to provide
   three required pieces of information: the name of the image that this container
   will be created from; the name of the container itself; and the absolute path
   to a folder that contains the source code. This folder will be mounted in the
   container, so there will be no need to copy files around. The folder can contain
   multiple worktrees and we will be able to build each one from inside the same
   container separately without clashes between builds:
   
   `./create_container [SRC_DIR] [IMAGE_NAME] [CONTAINER_NAME]`
   
   As with the image name, you can choose whatever name you want for the container.
   I like to use something like `krita_2024-01-26`, where the date has the same
   meaning as in the image name. A possible command for creating the container
   would look like:
   
   `./create_container /home/user/dev/src/krita/ krita_dev_env_2024-01-26 krita_2024-01-26`
   
   You can also specify an additional folder that will be mounted in the container.
   For example, if you want some folder outside the source directory to be available
   inside the container with the purpose of sharing files back and forth. For that
   you have to set an additional option, named `shared`, like this:
   
   `./create_container /home/user/dev/src/krita/ krita_dev_env_2024-01-26 krita_2024-01-26 --shared=/home/user/some_path/`
   
   But you can also have a folder inside the source directory for that purpose, so...
   your choice.
   
   Keep in mind that you cannot change the source or shared directories after
   the container is created. For that, a new container would have to be created.
   
4. The environment is now set up. Every time you want to access the container
   you just execute:
   
   `./enter_container [CONTAINER_NAME]`
   
   where `[CONTAINER_NAME]` is the name of the container you want to enter and
   you will be sent inside.
   
5. Anytime you want to download new dependencies to build Krita against, all the
   previous steps have to be performed again. You can choose new image and
   container names so that the previous ones don't get overridden. For example,
   you can build a new branch against the new deps using the new image/container,
   while finishing some other work in the old container, with the old dependencies.
   Using the new container means you have to rebuild krita from scratch, so if you
   have some unfinished work that uses the old container then it is better to keep
   it around.
   
6. When you don't need an old image/container anymore, you can delete them using
   the docker commands:
   
   ```
   docker container stop [CONTAINER_NAME]
   docker container rm [CONTAINER_NAME]
   docker image rm [IMAGE_NAME]
   ```
   
Each one of the scripts will print some help if you just execute them without any
options or arguments.

Building Krita
==============

When you enter the container using `./enter_container [CONTAINER_NAME]`, the prompt
changes to inside the container an will be located by default in `/home/appimage/dev`.
`/home/appimage` is the user folder inside the container, and `/home/appimage/dev` is
the folder where all the development files will be stored. Inside `dev` there are
four folders and some scripts to ease the development. The folders are used for
the following:

* `/home/appimage/dev/src/` This is where the source directory was mounted. You can access
  it as any other folder with read/wite permissions. You can modify the code
  from outside, from the host os, or from inside the container, although right
  now there are no editors installed in the container.
  
  There can be multiple source trees inside this folder.
  
* `/home/appimage/dev/shared/` This is where the shared folder is mounted (if you
  specified one when creating the container). You have read/write permissions
  from inside the docker.
  
* `/home/appimage/dev/build/` This is where the build files are stored. The build
  files of each source tree that you build will be put inside, on a unique folder that
  has the same relative path as the source folder used.
  
* `/home/appimage/dev/install/` This is where the install files will end up. This
  folder contains two sub-folders:
  
  * `/home/appimage/dev/install/deps/` This is where the pre-built dependencies
    were copied.
  
  * `/home/appimage/dev/install/krita/` The install files of each build will be
    put inside here on a unique folder that has the same relative path as the source
    folder used.
    
You should not have to mess with the build or install folders unless you want to
clean them or something like that.

For the builds to be smooth and use those predefined folders on a tidy way, you
have to use the provided scripts:

1. Configure some worktree inside the `src` folder by using something like:
   
   `./configure [SUB_FOLDER]`
   
   where `[SUB_FOLDER]` is a path relative to the `src` folder. For example, if
   you have some worktree in `./src/my/branch/` then you have to use:
   
   `./configure my/branch`
   
   This will configure the build inside `./build/my/branch/`.
   
   You can set additional CMake options with `--options`.
   
2. Build Krita for the configured folder with:
   
   `./make my/branch`
   
   You can use `-jN` or `--jobs=N` to set the number of jobs. The default is taken
   from `nproc`. You can also set the target with `-t` or `--target`. For example:
   
   `./make my/branch -j4`
   
   `./make my/branch --target=test`
   
3. Run the tests with:
   
   `./make my/branch --target=test`
   
   or just:
   
   `./run_tests my/branch`
   
4. Run the built Krita with:
   
   `./run my/branch`
    
5. You can debug Krita using gdb with:
   
   `./debug my/branch`
   
6. And you can build an appimage with:
   
   `./build_appimage my/branch`
   
   The appimages are placed inside `/home/appimage/dev/appimage`
   