prepend() { [ -d "$2" ] && eval $1=\"$2\$\{$1:+':'\$$1\}\" && export $1 ; }

export DEPSDIR=/home/appimage/dev/install/deps/usr

prepend PATH $DEPSDIR/bin
prepend LD_LIBRARY_PATH $DEPSDIR/lib64
prepend LD_LIBRARY_PATH $DEPSDIR/lib
prepend LD_LIBRARY_PATH $DEPSDIR/lib/x86_64-linux-gnu
#prepend XDG_DATA_DIRS $DEPSDIR/share
prepend PKG_CONFIG_PATH $DEPSDIR/lib64/pkgconfig
prepend PKG_CONFIG_PATH $DEPSDIR/lib/pkgconfig

prepend CMAKE_PREFIX_PATH $DEPSDIR

#prepend PYTHONPATH $DEPSDIR/lib/python3.5/
prepend PYTHONPATH $DEPSDIR/sip

# Disable leak checks in case ASAN is used.
# It takes a lot of time to finalize the leaks 
# and we usually just check for invalid access
export ASAN_OPTIONS=detect_leaks=0
