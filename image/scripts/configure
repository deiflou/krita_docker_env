#!/bin/bash

usage=\
"Usage: $(basename "$0") [OPTIONS]... SUB-DIRECTORY

Run CMake for a specific folder in the source directory.
Specify a relative path to a sub-directory that contains the the source
code, for example: \"./configure deiflou/some_branch\". That way the
source code would be looked up in \"~/dev/src/deiflou/some_branch\", the
build files would be placed in \"~/dev/build/deiflou/some_branch\", and the
install directory would be \"~/dev/install/krita/deiflou/some_branch\".

Options:
    -h, --help       show this help text
    -o, --options    additional CMake arguments
"

function print_usage() {
    printf '%b\n' "${usage}"
}

# Call getopt to validate the provided input. 
options=$(getopt -o o:h --long options:,help -- "$@")

if [ $# -eq 0 ]; then
    print_usage
    exit 1
fi

eval set -- "$options"

EXTRA_CMAKE_ARGS=

while true; do
    case "$1" in
    -h | --help)
        print_usage
        exit 1
        ;;
    -u | --options)
        shift
        EXTRA_CMAKE_ARGS=$1
        ;;
    --)
        shift
        break
        ;;
    esac
    shift
done

SOURCE_SUBDIR=$1
SOURCE_DIR="/home/appimage/dev/src/${SOURCE_SUBDIR}/"

if [[ (-z ${SOURCE_SUBDIR}) || !(-d ${SOURCE_DIR}) ]]; then
    echo "Invalid source directory specified"
    echo
    print_usage
    exit 1
fi

BUILD_DIR="/home/appimage/dev/build/${SOURCE_SUBDIR}/"
INSTALL_DIR="/home/appimage/dev/install/krita/${SOURCE_SUBDIR}/usr/"
DEPS_DIR="/home/appimage/dev/install/deps/usr/"

prepend() { [ -d "$2" ] && eval $1=\"$2\$\{$1:+':'\$$1\}\" && export $1 ; }

export QML2_IMPORT_PATH=$INSTALL_DIR/lib/x86_64-linux-gnu/qml

prepend PATH $INSTALL_DIR/bin
prepend LD_LIBRARY_PATH $INSTALL_DIR/lib64
prepend LD_LIBRARY_PATH $INSTALL_DIR/lib
#prepend XDG_DATA_DIRS $INSTALL_DIR/share
prepend PKG_CONFIG_PATH $INSTALL_DIR/lib64/pkgconfig
prepend PKG_CONFIG_PATH $INSTALL_DIR/lib/pkgconfig

cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} \
      -DCMAKE_BUILD_TYPE=Debug \
      -DKRITA_DEVS=ON \
      -DBUILD_TESTING=TRUE \
      -DHIDE_SAFE_ASSERTS=FALSE \
      -DPYQT_SIP_DIR_OVERRIDE=$DEPS_DIR/share/sip \
      -B ${BUILD_DIR} \
      -S ${SOURCE_DIR}
